<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class att_report extends CI_Controller {
	
	public function __construct(){
		parent:: __construct();
		$this->load->model('att_report_model');

	} 
	
	public function index(){

		if($this->input->post()){
			$post_data = $this->input->post();
			$data['reports']= $this->att_report_model->getStudent($post_data);
			echo $this->db->last_query();
			exit;
			$data['classes'] = $this->db->get_where('classes', array('ClassStatus'=>1))->result();
			$data['page_title'] = 'Students Attendance:: School management system'; 
			$this->load->view('include/header',$data);
			$this->load->view('att_report/index');
			$this->load->view('include/footer');
		}else{
			$data['classes'] = $this->db->get_where('classes', array('ClassStatus'=>1))->result();
			$data['page_title'] = 'Students Attendance:: School management system'; 
			$this->load->view('include/header',$data);
			$this->load->view('att_report/index');
			$this->load->view('include/footer');
		}
			
  }
  	
}