<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends CI_Controller {
	
	public function __construct(){
		parent:: __construct();
		$this->load->model(array('classes_model'));
		if(!$this->session->userdata('is_logged')){
			redirect('welcome');
		}
	}
	public function index(){
		$this->db->order_by('id','DSCE');
	 	$data['templates'] = $this->db->get_where('sms_templates', array('status='=>1));
		$data['page_title'] = 'SMS Create::TiifinBox';
		$this->load->view('include/header',$data);
		$this->load->view('sms/create');
		$this->load->view('include/footer');
	}
 	public function send(){
		$data = $this->input->post();
		var_dump($data); 
	}
	public function provider(){
		$data['page_title'] = 'SMS Provider::TiifinBox';
		$data['provider'] = $this->db->get('sms_provider');
		$this->load->view('include/header',$data);
		$this->load->view('sms/provider');
		$this->load->view('include/footer');
	}
	 public function templates(){
	 	$data['page_title'] = 'SMS Templates::TiifinBox';
	 	$this->db->order_by('id','DSCE');
	 	$data['templates'] = $this->db->get_where('sms_templates', array('status='=>1));
	 	$this->load->view('include/header',$data);
		$this->load->view('sms/templates');
		$this->load->view('include/footer');
	 }
	 public function template_create(){ 
		$data['page_title'] = 'Add SMS Templates::TiifinBox';
 		$this->load->view('include/header',$data);
		$this->load->view('sms/template_create');
		$this->load->view('include/footer');
		
	 
	 }
	 public function insert_template(){
	 	 if($this->db->insert('sms_templates', $this->input->post())){
			$this->session->set_flashdata('status_right', 'New Template Create Complete!');
					redirect ('sms/templates');
	 	 }else{
			$this->session->set_flashdata('status_wrong', 'Sorry system is unable to preserve this info!');
					redirect('sms/create');
	 	 } 	  
	}

	public function template_update($id){ 
		$data['template'] = $this->db->get_where('sms_templates', array('id'=>$id));
		$data['page_title'] = 'Update SMS Templates :: TiffinBox';
		$this->load->view('include/header',$data);
		$this->load->view('sms/template_update');
		$this->load->view('include/footer');
	}

	public function edit_template($id){
		$this->db->where('id', $id);
		if($this->db->update('sms_templates', $this->input->post())){
			$this->session->set_flashdata('status_right', 'Template Updated!');
					redirect ('sms/templates');
	 	 }else{
			$this->session->set_flashdata('status_wrong', 'Sorry system is unable to preserve this info!');
					redirect('sms/update/'.$id);
	 	 }
	}

	public function template_delete($id){
		$this->db->where('id', $id);
		if($this->db->update('sms_templates', array('status'=>-1))){
			$this->session->set_flashdata('status_right', 'Successfully delete template!');
					redirect ('sms/templates');
	 	 }else{
			$this->session->set_flashdata('status_wrong', 'Sorry system is unable to preserve this info!');
					redirect ('sms/templates');
	 	 }
	}
}	 
