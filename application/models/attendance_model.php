<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Attendance_Model extends CI_Model {

	public function __construct()
	{
		$this->load->library('session');
	}
	public function getStudent($data){
		$this->db->select('*');
		$this->db->from('studentinfo as info');
		$this->db->join('studentdetails as detail', 'detail.Id=info.StdDetailsId ');
		$this->db->where('info.StdClassId', $data['classId']);
		if($data['SectionId']){
			$this->db->where('info.StdSectionId', $data['SectionId']);
		}
		$this->db->order_by('info.StdRollNo', 'asc'); 
		return  $this->db->get();
		  
	}




	public function getAbsenceStudent($data){
		$date = date('Y-m-d', strtotime($data['date']));
		if($data['SectionId']){
			$join_cluse = 'info.StdSectionId=att.sectionId';
		}else{
			$join_cluse = '1=1';
		}
		$this->db->select('*');
		$this->db->from('attendance as att');

		$this->db->join('studentinfo as info', 'info.StdRollNo=att.rollNo and info.StdClassId=att.classId AND '.$join_cluse);
		$this->db->join('studentdetails as detail', 'info.StdDetailsId = detail.Id');
	 	$this->db->where(array('date' => $date, )); 
		$this->db->order_by('info.StdRollNo', 'asc'); 
		//$this->db->get();
		//echo $this->db->last_query();
		//exit;
		return  $this->db->get();





		// $this->db->select('*');
		// $this->db->from('studentinfo as info');
		// $this->db->join('studentdetails as detail', 'detail.Id=info.StdDetailsId ');
		// $this->db->where('info.StdClassId', $data['classId']);
		// if($data['SectionId']){
		// 	$this->db->where('info.StdSectionId', $data['SectionId']);
		// }
		// $this->db->order_by('info.StdRollNo', 'asc'); 
		// return  $this->db->get();
		  
	}
	
	 
}